import { Component } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'lb-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent {
  constructor(private authService: AuthService) { }

  get fullName() {
    return this.authService.getFullName();
  }

  get username() {
    return this.authService.getUsername();
  }

  get firstName() {
    return this.authService.getFirstName();
  }
}
