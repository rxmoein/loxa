import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { forkJoin, lastValueFrom, map } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { CategorySelectorModalComponent } from '../../shared/modals/category-selector-modal';
import { AudioForMultiTrackModalComponent } from '../shared/audio-for-multi-track-modal';
import { GenreSelectorModalComponent } from 'src/app/shared/modals/genre-selector-modal';
import { CustomErrorHandler } from 'src/app/core/services/custom-error-handler.service';
import { TagSelectorModalComponent } from 'src/app/shared/modals/tag-selector-modal';
import { ConfirmModalComponent } from 'src/app/shared/modals/confirm-modal';
import { SnackbarService } from 'src/app/core/services/snackbar.service';
import { CategoryService } from 'src/app/core/services/category.service';
import { AlertModalComponent } from 'src/app/shared/modals/alert-modal';
import { PostReviewStatus, PostStatus } from 'src/app/core/models/post';
import { GenresService } from 'src/app/core/services/genres.service';
import { MediaService } from 'src/app/core/services/media.service';
import { UtilsService } from 'src/app/core/services/utils.service';
import { Audio, Media, Picture } from 'src/app/core/models/media';
import { PostService } from 'src/app/core/services/post.service';
import { AuthService } from 'src/app/core/services/auth.service';
import { TagsService } from 'src/app/core/services/tags.service';
import { CustomResponse } from 'src/app/core/models/response';
import { Category } from 'src/app/core/models/category';
import { TusdUpload } from 'src/app/core/models/tusd';
import { Genre } from 'src/app/core/models/genres';
import { Tag } from 'src/app/core/models/tags';
import { Post } from '../../core/models/post';

@Component({
  selector: 'lb-track',
  templateUrl: './track.component.html',
  styleUrls: ['./track.component.scss']
})
export class TrackComponent implements OnInit {
  form = new UntypedFormGroup({});
  loading = false;
  hasError = false;
  submitting = false

  categories: Category[] = [];
  selectedCategories: Category[] = [];

  genres: Genre[] = [];
  selectedGenres: Genre[] = [];

  tags: Tag[] = [];
  selectedTags: Tag[] = [];

  audioMediaUploads: TusdUpload[] = [];
  pictureMediaUploads: TusdUpload[] = [];
  multiTrackMediaUploads: TusdUpload[] = [];

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private fb: UntypedFormBuilder,
    private tagService: TagsService,
    private authService: AuthService,
    private postService: PostService,
    private snackbar: SnackbarService,
    private mediaService: MediaService,
    private genreService: GenresService,
    private categoryService: CategoryService,
    private errorHandler: CustomErrorHandler,
  ) {
    this.form = this.fb.group({
      title: ['', Validators.required],
      code: [this.generateCode(), Validators.required],
      price: [0],
      description: [''],
      status: [PostStatus.Draft],
      review_status: [PostReviewStatus.None],
    });
  }

  ngOnInit(): void {
    this.getData();
  }

  async getData(): Promise<void> {
    try {
      this.hasError = false;
      this.loading = true;
      const request$ = forkJoin([
        this.categoryService.getCategories(),
        this.genreService.getGenres(),
        this.tagService.getTags(),
      ]);
      const [categoryResult, genreResult, tagResult] = await lastValueFrom(request$);
      this.categories = categoryResult.result;
      this.genres = genreResult.result;
      this.tags = tagResult.result;
      this.hasError = false;
    } catch (error) {
      this.hasError = true;
      this.errorHandler.handle(error);
    }
  }

  private generateCode(): string {
    const date = new Date();
    const ms = (+date).toString();
    return `BK${date.getUTCMonth() + 1}T${ms.slice(8)}`;
  }

  onCategories(): void {
    const ref = this.dialog.open(CategorySelectorModalComponent, {
      width: '400px',
      data: {
        categories: this.categories,
        selectedCategories: this.selectedCategories,
      },
      autoFocus: false,
    });

    ref.afterClosed().subscribe(res => {
      if (res && res.submit) {
        this.selectedCategories = res.result || [];
      }
    });
  }

  selectedCategoriesText(count: number): string {
    return UtilsService.abbrTitlesText(this.selectedCategories, count);
  }

  onGenre(): void {
    const ref = this.dialog.open(GenreSelectorModalComponent, {
      width: '400px',
      data: {
        genres: this.genres,
        selectedGenres: this.selectedGenres,
      },
      autoFocus: false,
    });

    ref.afterClosed().subscribe(res => {
      if (res && res.submit) {
        this.selectedGenres = res.result || [];
      }
    });
  }

  selectedGenresText(count: number): string {
    return UtilsService.abbrTitlesText(this.selectedGenres, count);
  }

  onTag(): void {
    const ref = this.dialog.open(TagSelectorModalComponent, {
      width: '400px',
      data: {
        tags: this.tags,
        selectedTags: this.selectedTags,
      },
      autoFocus: false,
    });

    ref.afterClosed().subscribe(res => {
      if (res && res.submit) {
        this.selectedTags = res.result || [];
      }
    });
  }

  selectedTagsText(count: number): string {
    return UtilsService.abbrTitlesText(this.selectedTags, count);
  }

  onAudioFileInputChange(event: any): void {
    if (event?.target?.files?.length) {
      const file = event?.target?.files[0];
      const upload = new TusdUpload(file, this.generateCode());
      this.audioMediaUploads.push(upload);
    }
  }

  removeAudioMediaUpload(index: number): void {
    const ref = this.dialog.open(ConfirmModalComponent, {
      width: '400px',
      data: {
        title: 'Remove Audio',
        message: 'Are you sure about removing this uploaded audio file?'
      }
    });
    ref.afterClosed().subscribe(res => {
      if (res) {
        this.audioMediaUploads.splice(index, 1);
      }
    });
  }

  onPictureFileInputChange(event: any) {
    if (event?.target?.files?.length) {
      const file = event?.target?.files[0];
      const upload = new TusdUpload(file, this.generateCode());
      this.pictureMediaUploads = [upload];
    }
  }

  removePictureMediaUpload(index: number) {
    const ref = this.dialog.open(ConfirmModalComponent, {
      width: '400px',
      data: {
        title: 'Remove Picture',
        message: 'Are you sure about removing this uploaded picture file?'
      }
    });
    ref.afterClosed().subscribe(res => {
      if (res) {
        this.pictureMediaUploads.splice(index, 1);
      }
    });
  }

  onMultiTrackFileInputChange(event: any) {
    if (this.audioMediaUploads.length === 1) {
      const file = event?.target?.files[0];
      const upload = new TusdUpload(file, this.generateCode());
      upload.parent = this.audioMediaUploads[0].code;
      this.multiTrackMediaUploads.push(upload);
      return;
    }

    const ref = this.dialog.open(AudioForMultiTrackModalComponent, {
      width: '400px',
      data: this.audioMediaUploads,
    });

    ref.afterClosed().subscribe(res => {
      if (res) {
        if (event?.target?.files?.length) {
          const file = event?.target?.files[0];
          const upload = new TusdUpload(file, this.generateCode());
          upload.parent = res;
          this.multiTrackMediaUploads.push(upload);
        }
      }
    });
  }

  removeMultiTrackMediaUpload(index: number) {
    const ref = this.dialog.open(ConfirmModalComponent, {
      width: '400px',
      data: {
        title: 'Remove MultiTrack',
        message: 'Are you sure about removing this uploaded compressed file?'
      }
    });
    ref.afterClosed().subscribe(res => {
      if (res) {
        this.multiTrackMediaUploads.splice(index, 1);
      }
    });
  }

  onAddZip(input: any) {
    if (!this.audioMediaUploads.length) {
      this.dialog.open(AlertModalComponent, {
        width: '400px',
        data: {
          title: 'No Audio Yet',
          message: 'Please first upload an audio so you can upload a multi track file for it.'
        }
      });
    } else {
      input.click();
    }
  }

  onCancel() {
    const ref = this.dialog.open(ConfirmModalComponent, {
      width: '400px',
      data: {
        title: 'Going Back',
        message: 'Are you sure about going back? all data will be discarded!'
      }
    });
    ref.afterClosed().subscribe(res => {
      if (res) {
        this.router.navigate(['/']);
      }
    });
  }

  async onSubmit() {
    if (this.form.invalid) {
      this.dialog.open(AlertModalComponent, {
        width: '400px',
        data: {
          title: 'Invalid Form',
          message: 'PLease review the form and enter the valid information.'
        }
      });
      return;
    }

    if (!this.selectedCategories.length) {
      this.dialog.open(AlertModalComponent, {
        width: '400px',
        data: {
          title: 'Select Category',
          message: 'Please select at least one category!'
        }
      });
      return;
    }

    if (!this.selectedGenres.length) {
      this.dialog.open(AlertModalComponent, {
        width: '400px',
        data: {
          title: 'Select Genre',
          message: 'Please select at least one genre!'
        }
      });
      return;
    }

    if (!this.audioMediaUploads.length) {
      this.dialog.open(AlertModalComponent, {
        width: '400px',
        data: {
          title: 'No Audio?',
          message: 'Please add a audio for your track.'
        }
      });
      return;
    }

    if (!this.pictureMediaUploads.length) {
      this.dialog.open(AlertModalComponent, {
        width: '400px',
        data: {
          title: 'No Cover?',
          message: 'Please add a picture for your track.'
        }
      });
      return;
    }

    if (this.form.value.price && !this.multiTrackMediaUploads.length) {
      this.dialog.open(AlertModalComponent, {
        width: '400px',
        data: {
          title: 'MultiTrack Required',
          message: 'To sell a beat you need to upload the multi track file too!'
        }
      });
      return;
    }

    for (const amu of [...this.audioMediaUploads, ...this.pictureMediaUploads, ...this.multiTrackMediaUploads]) {
      let value = 0;
      amu.progress.subscribe(p => value = p).unsubscribe();
      if (value !== 100) {
        this.dialog.open(AlertModalComponent, {
          width: '400px',
          data: {
            title: 'In Progress Upload',
            message: 'Upload operation is oin progress. Please submit when the upload operations are finished'
          }
        });
        return;
      }
    }

    this.submitting = true;

    // Upload the uploader files to storage and get the audio media object 
    const audioRequestArr = this.audioMediaUploads.map(amu => {
      return this.mediaService.uploadByURL(amu.upload.url, amu.file.name).pipe(
        map(v => {
          return {
            code: v.code,
            ok: v.ok,
            result: {
              ...v.result,
              // Keep the codes to keep the relations between audio and multi track
              _code: amu.code,
              _parent: amu.parent,
            }
          } as CustomResponse<Media>;
        })
      );
    });

    // Upload the uploader files to storage and get the picture media object 
    const pictureRequestArr = this.pictureMediaUploads.map(amu => {
      return this.mediaService.uploadByURL(amu.upload.url, amu.file.name);
    });

    // Upload the uploader files to storage and get the zip media object 
    const zipRequestArr = this.multiTrackMediaUploads.map(amu => {
      return this.mediaService.uploadByURL(amu.upload.url, amu.file.name).pipe(
        map(v => {
          return {
            code: v.code,
            ok: v.ok,
            result: {
              ...v.result,
              // Keep the codes to keep the relations between audio and multi track
              _code: amu.code,
              _parent: amu.parent,
            }
          } as CustomResponse<Media>;
        }),
      );
    });

    const mediaRequest$ = forkJoin([
      ...audioRequestArr,
      ...pictureRequestArr,
      ...zipRequestArr,
    ]);

    let mediaResponse: CustomResponse<Media>[] = [];

    try {
      mediaResponse = await lastValueFrom(mediaRequest$);
    } catch (error) {
      this.dialog.open(AlertModalComponent, {
        width: '400px',
        data: {
          title: 'Error!',
          message: 'Unfortunately something went wrong while storing your files. You can try again too. If this does not work, we will try to fix it ASAP!'
        }
      });
      this.submitting = false;
      return
    }
    const medias = mediaResponse.map(mr => mr.result);

    const audios: Audio[] = medias.filter(m => m.mime_type.startsWith('audio')).map(m => {
      const multiTracks = medias.filter(mt => mt.mime_type.includes('zip'));
      const multiTrackForThisAudio = multiTracks.filter(mt => mt._parent === m._code);

      return {
        title: m.name,
        original: m.uuid,
        multi_track: multiTrackForThisAudio.length ? multiTrackForThisAudio[0].uuid : undefined,
      }
    });

    const pictures: Picture[] = medias.filter(m => m.mime_type.startsWith('image')).map(m => {
      return {
        title: m.name,
        original: m.uuid,
        thumbnail: m.uuid,
        default: m.uuid,
      }
    });

    const data: Post = {
      id: 0,
      post_meta: {
        id: 0,
        description: this.form.value.description,
        price: this.form.value.price,
        title: this.form.value.title,
        code: this.form.value.code,
        status: this.form.value.status,
        review_status: this.form.value.review_status,
      },
      audios,
      pictures,
      categories: this.selectedCategories,
      genres: this.selectedGenres,
      tags: this.selectedTags,
    };

    try {
      await lastValueFrom(this.postService.createPost(data));
    } catch (error) {
      this.dialog.open(AlertModalComponent, {
        width: '400px',
        data: {
          title: 'Error!',
          message: 'Unfortunately something went wrong while sending the information. You can try again too. If this does not work, we will try to fix it ASAP!'
        }
      });
      this.submitting = false;
      return
    }
    this.snackbar.info('Your post submitted successfully!');
    this.router.navigate(['u', this.authService.getUsername()]);
    this.submitting = false;
  }
}
