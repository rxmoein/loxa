import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { TrackComponent } from './track/track.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'track',
      },
      {
        path: 'track',
        component: TrackComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UploadRoutingModule { }
