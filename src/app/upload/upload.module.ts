import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { UploadMediaItemComponent } from './shared/upload-media-item/upload-media-item.component';
import { AudioForMultiTrackModalComponent } from './shared/audio-for-multi-track-modal/audio-for-multi-track-modal.component';
import { UploadRoutingModule } from './upload-routing.module';
import { TrackComponent } from './track/track.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    TrackComponent,
    UploadMediaItemComponent,
    AudioForMultiTrackModalComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    UploadRoutingModule
  ]
})
export class UploadModule { }
