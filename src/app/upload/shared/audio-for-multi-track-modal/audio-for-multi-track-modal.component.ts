import { Component, Inject, Optional } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TusdUpload } from 'src/app/core/models/tusd';

@Component({
  selector: 'lb-audio-for-multi-track-modal',
  templateUrl: './audio-for-multi-track-modal.component.html',
  styleUrls: ['./audio-for-multi-track-modal.component.scss']
})
export class AudioForMultiTrackModalComponent {
  constructor(@Optional() @Inject(MAT_DIALOG_DATA) public data: TusdUpload[]) { }
}
