import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AudioForMultiTrackModalComponent } from './audio-for-multi-track-modal.component';

describe('AudioForMultiTrackModalComponent', () => {
  let component: AudioForMultiTrackModalComponent;
  let fixture: ComponentFixture<AudioForMultiTrackModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AudioForMultiTrackModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AudioForMultiTrackModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
