import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadMediaItemComponent } from './upload-media-item.component';

describe('UploadMediaItemComponent', () => {
  let component: UploadMediaItemComponent;
  let fixture: ComponentFixture<UploadMediaItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UploadMediaItemComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UploadMediaItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
