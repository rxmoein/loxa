import { AfterViewInit, Component, Input } from '@angular/core';
import { TusdUpload } from 'src/app/core/models/tusd';

@Component({
  selector: 'lb-upload-media-item',
  templateUrl: './upload-media-item.component.html',
  styleUrls: ['./upload-media-item.component.scss']
})
export class UploadMediaItemComponent implements AfterViewInit {
  @Input() uploadMedia?: TusdUpload;

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.uploadMedia?.start();
    }, 0);
  }
}
