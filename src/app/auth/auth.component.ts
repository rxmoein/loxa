import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'lb-auth',
  templateUrl: './auth.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class AuthComponent { }
