import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { EmailActivationFailedComponent } from './activation/email-activation-failed.component';
import { AuthContainerComponent } from './shared/auth-container/auth-container.component';
import { AuthContentComponent } from './shared/auth-content/auth-content.component';
import { AuthFooterComponent } from './shared/auth-footer/auth-footer.component';
import { ResetPasswordFailedComponent } from './forgot/forgot-failed.component';
import { EmailActivatedComponent } from './activation/email-activated.component';
import { ResetPasswordComponent } from './forgot/reset-password.component';
import { ForgotPasswordComponent } from './forgot/forgot.component';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { AuthRoutingModule } from './auth-routing.module';
import { SharedModule } from '../shared/shared.module';
import { AuthComponent } from './auth.component';

@NgModule({
  declarations: [
    AuthComponent,
    SigninComponent,
    SignupComponent,
    ForgotPasswordComponent,
    EmailActivatedComponent,
    ResetPasswordComponent,
    ResetPasswordFailedComponent,
    EmailActivationFailedComponent,
    AuthContainerComponent,
    AuthContentComponent,
    AuthFooterComponent
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    SharedModule,
  ]
})
export class AuthModule { }
