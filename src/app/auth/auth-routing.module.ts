import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { ResetPasswordFailedComponent } from './forgot/forgot-failed.component';
import { EmailActivationFailedComponent } from './activation/email-activation-failed.component';
import { EmailActivatedComponent } from './activation/email-activated.component';
import { ResetPasswordComponent } from './forgot/reset-password.component';
import { ForgotPasswordComponent } from './forgot/forgot.component';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { AuthGuard } from '../core/guards/auth.guard';
import { AuthComponent } from './auth.component';

const routes: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'signin',
      },
      {
        path: 'signin',
        canActivate: [AuthGuard],
        component: SigninComponent,
      },
      {
        path: 'signup',
        canActivate: [AuthGuard],
        component: SignupComponent,
      },
      {
        path: 'forgot',
        component: ForgotPasswordComponent,
      },
      {
        path: 'reset-password/:token',
        component: ResetPasswordComponent,
      },
      {
        path: 'email-activated',
        component: EmailActivatedComponent,
      },
      {
        path: 'reset-password-failed',
        component: ResetPasswordFailedComponent,
      },
      {
        path: 'email-activation-failed',
        component: EmailActivationFailedComponent,
      }
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
