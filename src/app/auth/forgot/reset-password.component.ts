import { CustomErrorHandler } from 'src/app/core/services/custom-error-handler.service';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { SnackbarService } from 'src/app/core/services/snackbar.service';
import { AuthService } from 'src/app/core/services/auth.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { lastValueFrom } from 'rxjs';

@Component({
  selector: 'lb-reset-password',
  templateUrl: './reset-password.component.html',
})
export class ResetPasswordComponent implements OnInit {
  form = new UntypedFormGroup({});
  isLoading = false;
  done = true;
  token = '';

  constructor(
    private route: ActivatedRoute,
    private fb: UntypedFormBuilder,
    private authService: AuthService,
    private snackbar: SnackbarService,
    private errorHandler: CustomErrorHandler,
  ) {
    const routeParams = this.route.snapshot.paramMap;
    this.token = routeParams.get('token') as string;
  }

  ngOnInit(): void {
    this.createForm();
  }

  createForm() {
    this.form = this.fb.group({
      password: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required]],
    });
  }

  async onSubmit(): Promise<void> {
    if (this.form.invalid) {
      return this.snackbar.error('Please fill the form with valid information!')
    }

    if (this.form.value.password !== this.form.value.confirmPassword) {
      this.form.markAllAsTouched();
      return this.snackbar.error('The passwords do not match! Try again.');
    }

    try {
      this.form.disable();
      this.isLoading = true;
      const request$ = this.authService.reset(this.form.value.password, this.token);
      await lastValueFrom(request$);
      this.isLoading = false;
      this.done = true;
    } catch (error) {
      this.form.enable();
      this.isLoading = false;
      this.errorHandler.handle(error);
    }
  }
}
