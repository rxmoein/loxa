import { CustomErrorHandler } from 'src/app/core/services/custom-error-handler.service';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { SnackbarService } from 'src/app/core/services/snackbar.service';
import { AuthService } from 'src/app/core/services/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'lb-forgot',
  templateUrl: './forgot.component.html',
})
export class ForgotPasswordComponent implements OnInit {
  form: UntypedFormGroup = new UntypedFormGroup({});
  isLoading = false;
  done = false;

  constructor(
    private fb: UntypedFormBuilder,
    private authService: AuthService,
    private snackbar: SnackbarService,
    private errorHandler: CustomErrorHandler,
  ) { }

  ngOnInit(): void {
    this.createForm();
  }

  private createForm(): void {
    this.form = this.fb.group({
      email: ['', Validators.required],
    });
  }

  async onSubmit(): Promise<void> {
    if (this.form.invalid) {
      return this.snackbar.error('Please fill the form with valid information!')
    }

    this.form.disable();
    this.isLoading = true;
    this.authService.forgot(this.form.value.email).subscribe({
      complete: () => {
        this.isLoading = false;
        this.done = true;
      },
      error: (error) => {
        this.form.enable();
        this.isLoading = false;
        this.errorHandler.handle(error);
      },
    });
  }
}
