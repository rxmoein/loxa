import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResetPasswordFailedComponent } from './forgot-failed.component';

describe('ResetPasswordFailedComponent', () => {
  let component: ResetPasswordFailedComponent;
  let fixture: ComponentFixture<ResetPasswordFailedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ResetPasswordFailedComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(ResetPasswordFailedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
