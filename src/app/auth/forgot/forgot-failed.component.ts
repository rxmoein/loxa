import { Component } from '@angular/core';

@Component({
  selector: 'lb-forgot-failed',
  templateUrl: './forgot-failed.component.html',
})
export class ResetPasswordFailedComponent { }
