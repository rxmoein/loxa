import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailActivatedComponent } from './email-activated.component';

describe('EmailActivatedComponent', () => {
  let component: EmailActivatedComponent;
  let fixture: ComponentFixture<EmailActivatedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmailActivatedComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EmailActivatedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
