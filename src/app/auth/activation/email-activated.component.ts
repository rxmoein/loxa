import { Component } from '@angular/core';

@Component({
  selector: 'lb-email-activated',
  templateUrl: './email-activated.component.html',
})
export class EmailActivatedComponent { }
