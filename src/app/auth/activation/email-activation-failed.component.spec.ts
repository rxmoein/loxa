import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailActivationFailedComponent } from './email-activation-failed.component';

describe('EmailActivationFailedComponent', () => {
  let component: EmailActivationFailedComponent;
  let fixture: ComponentFixture<EmailActivationFailedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmailActivationFailedComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EmailActivationFailedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
