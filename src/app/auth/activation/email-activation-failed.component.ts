import { Component } from '@angular/core';

@Component({
  selector: 'lb-email-activation-failed',
  templateUrl: './email-activation-failed.component.html',
})
export class EmailActivationFailedComponent { }
