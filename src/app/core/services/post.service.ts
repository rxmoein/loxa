import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { CustomResponse, PaginatedResponse } from '../models/response';
import { environment } from 'src/environments/environment';
import { Post } from '../models/post';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  constructor(private http: HttpClient) { }

  createPost(post: Post): Observable<CustomResponse<Post>> {
    return this.http.post<CustomResponse<Post>>(environment.contentUrl + '/posts', post);
  }

  updatePost(post: Post): Observable<CustomResponse<Post>> {
    return this.http.patch<CustomResponse<Post>>(environment.contentUrl + '/posts/' + post.id, post);
  }

  deletePost(postId: number): Observable<CustomResponse<any>> {
    return this.http.delete<CustomResponse<any>>(environment.contentUrl + '/posts/' + postId);
  }

  getPosts(pageSize = 10, page = 1, query = ''): Observable<PaginatedResponse<Post[]>> {
    const params = {
      page_size: pageSize,
      q: query,
      page,
    };
    return this.http.get<PaginatedResponse<Post[]>>(environment.contentUrl + '/posts', { params });
  }
}
