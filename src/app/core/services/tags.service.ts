import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { Tag } from '../models/tags';
import { CustomResponse } from '../models/response';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TagsService {

  constructor(private http: HttpClient) { }

  getTags(): Observable<CustomResponse<Tag[]>> {
    return this.http.get<CustomResponse<Tag[]>>(environment.contentUrl + '/tags');
  }

  createTags(tag: Tag): Observable<CustomResponse<Tag>> {
    return this.http.post<CustomResponse<Tag>>(environment.contentUrl + '/tags', tag);
  }

  editTags(id: number, tag: Tag): Observable<CustomResponse<Tag>> {
    tag.id = undefined as any;
    return this.http.patch<CustomResponse<Tag>>(environment.contentUrl + '/tags/' + id, tag);
  }

  deleteTags(id: number): Observable<CustomResponse<any>> {
    return this.http.delete<CustomResponse<any>>(environment.contentUrl + '/tags/' + id);
  }
}
