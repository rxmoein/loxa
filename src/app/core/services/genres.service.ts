import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { Genre } from '../models/genres';
import { CustomResponse } from '../models/response';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GenresService {

  constructor(private http: HttpClient) { }

  getGenres(): Observable<CustomResponse<Genre[]>> {
    return this.http.get<CustomResponse<Genre[]>>(environment.contentUrl + '/genres');
  }

  createGenres(genres: Genre): Observable<CustomResponse<Genre>> {
    return this.http.post<CustomResponse<Genre>>(environment.contentUrl + '/genres', genres);
  }

  editGenres(id: number, genres: Genre): Observable<CustomResponse<Genre>> {
    genres.id = undefined as any;
    return this.http.patch<CustomResponse<Genre>>(environment.contentUrl + '/genres/' + id, genres);
  }

  deleteGenres(id: number): Observable<CustomResponse<Genre>> {
    return this.http.delete<CustomResponse<Genre>>(environment.contentUrl + '/genres/' + id);
  }
}
