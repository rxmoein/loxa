import { LoginResponseDTO } from '../models/authentication';
import { environment } from 'src/environments/environment';
import { StorageKeys } from '../models/local-storage';
import { CustomResponse } from '../models/response';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(
    private router: Router,
    private http: HttpClient,
  ) { }

  login(identifier: string, password: string): Observable<CustomResponse<LoginResponseDTO>> {
    return this.http.post<CustomResponse<LoginResponseDTO>>(`${environment.authServiceURL}/auth/login`, { identifier, password });
  }

  forgot(email: string): Observable<CustomResponse<any>> {
    return this.http.post<CustomResponse<any>>(`${environment.authServiceURL}/auth/forgot-password`, { email });
  }

  reset(password: string, token: string): Observable<CustomResponse<any>> {
    return this.http.post<CustomResponse<any>>(`${environment.authServiceURL}/auth/reset-password`, { password, token }, {
      headers: {
        'Authorization': 'Bearer ' + token,
      },
    });
  }

  register(email: string, password: string): Observable<CustomResponse<any>> {
    return this.http.post<CustomResponse<any>>(`${environment.authServiceURL}/auth/register`, { email, password });
  }

  updateMyUser(username: string, first_name: string, last_name: string): Observable<CustomResponse<any>> {
    return this.http.patch<CustomResponse<any>>(`${environment.authServiceURL}/users/me`, { username, first_name, last_name });
  }

  getMyUser(): Observable<CustomResponse<any>> {
    return this.http.get<CustomResponse<any>>(`${environment.authServiceURL}/users/me`);
  }

  updateToken(token: string) {
    localStorage.setItem(StorageKeys.AuthToken, token);
  }

  getToken() {
    return localStorage.getItem(StorageKeys.AuthToken);
  }

  checkUsernameAvailability(username: string) {
    return this.http.get<CustomResponse<any>>(`${environment.authServiceURL}/users/check/username/` + username);
  }

  getUsername(): string {
    const username = localStorage.getItem(StorageKeys.UserUsername);
    return username || '';
  }


  getFirstName(): string {
    const firstName = localStorage.getItem(StorageKeys.UserFirstName);

    if (!firstName) {
      return localStorage.getItem(StorageKeys.UserUsername) || 'N/A';
    }

    return firstName;
  }


  getFullName(): string {
    const firstName = localStorage.getItem(StorageKeys.UserFirstName);
    const lastName = localStorage.getItem(StorageKeys.UserLastName);

    if (!firstName && !lastName) {
      return localStorage.getItem(StorageKeys.UserUsername) || 'N/A';
    }

    return `${firstName || ''} ${lastName || ''}`;
  }

  updateLocalUser(
    { id, firstName, lastName, roleId, status, username }: {
      id?: number,
      firstName?: string,
      lastName?: string,
      roleId?: number,
      status?: number,
      username?: string,
    }) {
    if (id) {
      localStorage.setItem(StorageKeys.UserId, String(id));
    }

    if (firstName) {
      localStorage.setItem(StorageKeys.UserFirstName, String(firstName));
    }

    if (lastName) {
      localStorage.setItem(StorageKeys.UserFirstName, String(lastName));
    }

    if (roleId) {
      localStorage.setItem(StorageKeys.UserRoleId, String(roleId));
    }

    if (status) {
      localStorage.setItem(StorageKeys.UserStatus, String(status));
    }

    if (username) {
      localStorage.setItem(StorageKeys.UserUsername, String(username));
    }
  }

  logout(): void {
    localStorage.clear();
    setTimeout(() => {
      this.router.navigate(['/authentication/signin']);
    }, 0);
  }
}
