import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';
import { CustomResponse } from '../models/response';
import { Category } from '../models/category';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  constructor(private http: HttpClient) { }

  getCategories(): Observable<CustomResponse<Category[]>> {
    return this.http.get<CustomResponse<Category[]>>(environment.contentUrl + '/categories');
  }

  createCategory(category: Category): Observable<CustomResponse<Category>> {
    return this.http.post<CustomResponse<Category>>(environment.contentUrl + '/categories', category);
  }

  editCategory(id: number, category: Category): Observable<CustomResponse<Category>> {
    category.id = undefined as any;
    return this.http.patch<CustomResponse<Category>>(environment.contentUrl + '/categories/' + id, category);
  }

  deleteCategory(id: number): Observable<CustomResponse<any>> {
    return this.http.delete<CustomResponse<any>>(environment.contentUrl + '/categories/' + id);
  }
}
