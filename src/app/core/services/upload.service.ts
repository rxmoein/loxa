import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

declare var tus: any;

@Injectable({
  providedIn: 'root'
})
export class UploadService {
  constructor(private http: HttpClient) { }

  uploadFile(file: File) {
    const progress = new BehaviorSubject<any>(0);

    const u = new tus.Upload(file, {
      chunkSize: 2000_000,
      endpoint: environment.uploaderUrl,
      retryDelays: [0],
      metadata: {
        filename: file.name,
        filetype: file.type,
      },
      onError: (err: any) => {
        progress.error(err);
        progress.complete();
      },
      onProgress: (bytesUploaded: number, bytesTotal: number) => {
        const percentage = (bytesUploaded / bytesTotal * 100);
        const uploadProgress = Math.round(percentage);
        progress.next(uploadProgress);
      },
      onSuccess: () => {
        progress.next(100)
        progress.complete();
      },
    });
    u.start();
    return { upload: u, progress, file }
  }
}
