import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor() { }

  static abbrTitlesText(source: any[], count: number) {
    if (!source.length) {
      return '';
    }

    let text = '';
    for (let i = 0; i < source.length && i < count; i++) {
      const element = source[i];
      text += element.title;

      if (source.length > 1 && i < (count - 1)) {
        text += ', ';
      }
    }

    if (source.length > count) {
      text += ' and ' + (source.length - count) + ' more';
    }
    return text
  }
}
