import { ConfirmModalComponent } from 'src/app/shared/modals/confirm-modal/confirm-modal.component';
import { AuthService } from '../../services/auth.service';
import { MatDialog } from '@angular/material/dialog';
import { Component } from '@angular/core';

@Component({
  selector: 'lb-wrapper',
  templateUrl: './wrapper.component.html',
  styleUrls: ['./wrapper.component.scss']
})
export class WrapperComponent {
  isSidenavOpen = false;

  constructor(
    private dialog: MatDialog,
    private authService: AuthService,
  ) { }

  get firstName() {
    return this.authService.getFirstName();
  }

  get username() {
    return this.authService.getUsername();
  }

  onMenu() {
    this.isSidenavOpen = !this.isSidenavOpen;
  }

  onLogout() {
    const ref = this.dialog.open(ConfirmModalComponent, {
      width: '400px',
      autoFocus: false,
      data: {
        title: 'Logout',
        message: 'Are you sure about signing out of your account?'
      }
    });

    ref.afterClosed().subscribe(res => {
      if (res) {
        this.authService.logout();
      }
    });
  }
}
