import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { lastValueFrom } from 'rxjs';

import { ThemeService } from './core/services/theme.service';
import { AuthService } from './core/services/auth.service';

@Component({
  selector: 'lb-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(
    private router: Router,
    private authService: AuthService,
    private themeService: ThemeService,
  ) { }

  ngOnInit(): void {
    this.getData();
  }

  async getData(): Promise<void> {
    if (!this.authService.getToken()) {
      return;
    }

    try {
      const request$ = this.authService.getMyUser();
      const response = await lastValueFrom(request$);

      if (!response.result.profile_completed) {
        this.router.navigate(['routine', 'complete-profile']);
      }
    } catch (error) {
    }
  }

  get currentTheme() {
    return this.themeService.currentTheme;
  }
}
