import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoutineRoutingModule } from './routine-routing.module';
import { CompleteProfileComponent } from './complete-profile/complete-profile.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    CompleteProfileComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RoutineRoutingModule
  ]
})
export class RoutineModule { }
