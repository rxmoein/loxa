import { AbstractControl, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { CustomErrorHandler } from 'src/app/core/services/custom-error-handler.service';
import { SnackbarService } from 'src/app/core/services/snackbar.service';
import { AuthService } from 'src/app/core/services/auth.service';
import { debounceTime, lastValueFrom, Subject } from 'rxjs';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'lb-complete-profile',
  templateUrl: './complete-profile.component.html',
  styleUrls: ['./complete-profile.component.scss']
})
export class CompleteProfileComponent {
  form: UntypedFormGroup = new UntypedFormGroup({});
  usernameTerm = new Subject();
  usernameLoading = false;
  isUsernameAvailable = true;
  isLoading = false;

  constructor(
    private router: Router,
    private fb: UntypedFormBuilder,
    private authService: AuthService,
    private snackbar: SnackbarService,
    private errHandler: CustomErrorHandler,
  ) {
    this.createForm();
  }

  private createForm(): void {
    this.form = this.fb.group({
      username: ['', [Validators.required, usernameValidator, Validators.minLength(4)]],
      first_name: ['', [Validators.required, Validators.maxLength(64)]],
      last_name: ['', [Validators.required, Validators.maxLength(64)]],
    });

    this.form.controls['username'].valueChanges.pipe(debounceTime(300)).subscribe({
      next: (value) => {
        this.usernameLoading = true;
        this.checkUsernameAvailability(value);
      },
    });
  }

  async checkUsernameAvailability(value: string): Promise<void> {
    try {
      const request$ = this.authService.checkUsernameAvailability(value);
      const result = await lastValueFrom(request$);
      this.isUsernameAvailable = result.result.available;
      this.usernameLoading = false;
    } catch (error) {
      this.usernameLoading = false;
    }
  }

  async onSubmit(): Promise<void> {
    if (this.form.invalid || !this.isUsernameAvailable) {
      return this.snackbar.error('Please fill the form with valid information!')
    }

    try {
      this.form.disable();
      this.isLoading = true;
      const request$ = this.authService.updateMyUser(
        this.form.value.username,
        this.form.value.first_name,
        this.form.value.last_name
      );

      await lastValueFrom(request$);

      this.authService.updateLocalUser({
        firstName: this.form.value.first_name,
        lastName: this.form.value.last_name,
        username: this.form.value.username,
      });
      this.isLoading = false;
      this.form.enable();
      this.router.navigate(['/']);
    } catch (error) {
      this.form.enable();
      this.isLoading = false;
      this.errHandler.handle(error);
    }
  }
}

function usernameValidator(control: AbstractControl): { [key: string]: boolean } | null {
  const usernameRegex = /^[a-z0-9_]+$/;
  const value = String(control.value || '').toLowerCase();
  if (value !== undefined && usernameRegex.test(value)) {
    return null;
  }

  return { 'usernameValid': true };
}