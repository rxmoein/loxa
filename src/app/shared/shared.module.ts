import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ConfirmModalComponent } from './modals/confirm-modal/confirm-modal.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { NotFoundComponent } from './not-found/not-found.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatChipsModule } from '@angular/material/chips';
import { HttpClientModule } from '@angular/common/http';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { RouterModule } from '@angular/router';
import { NavbarOptionDirective } from './ui-kit/lb-navbar-option/navbar-option.directive';
import { CategorySelectorModalComponent } from './modals/category-selector-modal/category-selector-modal.component';
import { GenreSelectorModalComponent } from './modals/genre-selector-modal/genre-selector-modal.component';
import { AlertModalComponent } from './modals/alert-modal/alert-modal.component';
import { TagSelectorModalComponent } from './modals/tag-selector-modal/tag-selector-modal.component';

const matModules = [
  MatSnackBarModule,
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatIconModule,
  MatProgressSpinnerModule,
  MatToolbarModule,
  MatDialogModule,
  MatListModule,
  MatChipsModule,
  MatAutocompleteModule,
  MatProgressBarModule,
  MatSidenavModule,
  MatMenuModule,
  MatCheckboxModule,
  MatTabsModule,
];


@NgModule({
  declarations: [
    NotFoundComponent,
    ConfirmModalComponent,
    NavbarOptionDirective,
    CategorySelectorModalComponent,
    GenreSelectorModalComponent,
    AlertModalComponent,
    TagSelectorModalComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    HttpClientModule,
    ReactiveFormsModule,
    ...matModules,
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    ConfirmModalComponent,
    NavbarOptionDirective,
    ...matModules,
  ],
})
export class SharedModule { }
