import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TagSelectorModalComponent } from './tag-selector-modal.component';

describe('TagSelectorModalComponent', () => {
  let component: TagSelectorModalComponent;
  let fixture: ComponentFixture<TagSelectorModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TagSelectorModalComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(TagSelectorModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
