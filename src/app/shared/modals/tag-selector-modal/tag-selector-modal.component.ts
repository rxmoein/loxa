import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, Inject, OnInit, Optional } from '@angular/core';
import { Tag } from 'src/app/core/models/tags';
import { Subject } from 'rxjs';

@Component({
  selector: 'lb-tag-selector-modal',
  templateUrl: './tag-selector-modal.component.html',
  styleUrls: ['./tag-selector-modal.component.scss']
})
export class TagSelectorModalComponent implements OnInit {
  queryChanges: Subject<string> = new Subject<string>();
  filteredTags: Tag[] = [];
  selectedTags: Tag[] = [];
  tags: Tag[] = [];
  loading = false;
  query = '';

  constructor(
    public dialogRef: MatDialogRef<TagSelectorModalComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public initializedData: {
      selectedTags: Tag[],
      tags: Tag[],
    },
  ) { }

  ngOnInit(): void {
    this.selectedTags = JSON.parse(JSON.stringify(this.initializedData.selectedTags));
    this.getData();
    this.queryChanges.subscribe(() => {
      let temp = [];
      if (!this.query) {
        temp = this.tags.slice(0, 10);
      } else {
        const q = this.query.toLowerCase();
        temp = this.tags.filter(c => {
          return c.title.toLowerCase().includes(q);
        }).slice(0, 10);
      }

      for (const tempC of temp) {
        for (const selectedC of this.selectedTags) {
          if (tempC.id === selectedC.id) {
            tempC._selected = true;
            break;
          }
        }
      }

      this.filteredTags = temp;
    });
  }

  async getData() {
    this.tags = this.initializedData.tags;
    this.filteredTags = this.tags.slice(0, 10);
    for (const tempC of this.filteredTags) {
      for (const selectedC of this.selectedTags) {
        if (tempC.id === selectedC.id) {
          tempC._selected = true;
          break;
        }
      }
    }
  }

  queryChange() {
    this.queryChanges.next(this.query);
  }

  selectionChange(tag: Tag, value: boolean) {
    if (value) {
      const filtered = this.selectedTags.filter(c => c.id === tag.id);
      if (!filtered.length) {
        this.selectedTags.push(tag);
      }
    } else {
      this.selectedTags = this.selectedTags.filter(c => c.id !== tag.id);
    }
  }

  close(submit: boolean, result?: any) {
    this.dialogRef.close({
      submit,
      result,
    });
  }
}
