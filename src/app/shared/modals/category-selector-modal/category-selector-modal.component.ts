import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, Inject, OnInit, Optional } from '@angular/core';
import { Category } from 'src/app/core/models/category';
import { Subject } from 'rxjs';

@Component({
  selector: 'lb-category-selector-modal',
  templateUrl: './category-selector-modal.component.html',
  styleUrls: ['./category-selector-modal.component.scss']
})
export class CategorySelectorModalComponent implements OnInit {
  queryChanges: Subject<string> = new Subject<string>();
  filteredCategories: Category[] = [];
  selectedCategories: Category[] = [];
  categories: Category[] = [];
  loading = false;
  query = '';

  constructor(
    public dialogRef: MatDialogRef<CategorySelectorModalComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public initializedData: {
      selectedCategories: Category[],
      categories: Category[],
    },
  ) { }

  ngOnInit(): void {
    this.selectedCategories = JSON.parse(JSON.stringify(this.initializedData.selectedCategories));
    this.getData();
    this.queryChanges.subscribe(() => {
      let temp = [];
      if (!this.query) {
        temp = this.categories.slice(0, 10);
      } else {
        const q = this.query.toLowerCase();
        temp = this.categories.filter(c => {
          return c.title.toLowerCase().includes(q);
        }).slice(0, 10);
      }

      for (const tempC of temp) {
        for (const selectedC of this.selectedCategories) {
          if (tempC.id === selectedC.id) {
            tempC._selected = true;
            break;
          }
        }
      }

      this.filteredCategories = temp;
    });
  }

  async getData() {
    this.categories = this.initializedData.categories;
    this.filteredCategories = this.categories.slice(0, 10);
    for (const tempC of this.filteredCategories) {
      for (const selectedC of this.selectedCategories) {
        if (tempC.id === selectedC.id) {
          tempC._selected = true;
          break;
        }
      }
    }
  }

  queryChange() {
    this.queryChanges.next(this.query);
  }

  selectionChange(category: Category, value: boolean) {
    if (value) {
      const filtered = this.selectedCategories.filter(c => c.id === category.id);
      if (!filtered.length) {
        this.selectedCategories.push(category);
      }
    } else {
      this.selectedCategories = this.selectedCategories.filter(c => c.id !== category.id);
    }
  }

  close(submit: boolean, result?: any) {
    this.dialogRef.close({
      submit,
      result,
    });
  }
}
