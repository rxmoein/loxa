import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategorySelectorModalComponent } from './category-selector-modal.component';

describe('CategorySelectorModalComponent', () => {
  let component: CategorySelectorModalComponent;
  let fixture: ComponentFixture<CategorySelectorModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategorySelectorModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CategorySelectorModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
