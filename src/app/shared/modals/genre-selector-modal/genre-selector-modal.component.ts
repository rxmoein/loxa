import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, Inject, OnInit, Optional } from '@angular/core';
import { Genre } from 'src/app/core/models/genres';
import { Subject } from 'rxjs';

@Component({
  selector: 'lb-genre-selector-modal',
  templateUrl: './genre-selector-modal.component.html',
  styleUrls: ['./genre-selector-modal.component.scss']
})
export class GenreSelectorModalComponent implements OnInit {
  queryChanges: Subject<string> = new Subject<string>();
  filteredGenres: Genre[] = [];
  selectedGenres: Genre[] = [];
  genres: Genre[] = [];
  loading = false;
  query = '';

  constructor(
    public dialogRef: MatDialogRef<GenreSelectorModalComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public initializedData: {
      selectedGenres: Genre[],
      genres: Genre[],
    },
  ) { }

  ngOnInit(): void {
    this.selectedGenres = JSON.parse(JSON.stringify(this.initializedData.selectedGenres));
    this.getData();
    this.queryChanges.subscribe(() => {
      let temp = [];
      if (!this.query) {
        temp = this.genres.slice(0, 10);
      } else {
        const q = this.query.toLowerCase();
        temp = this.genres.filter(c => {
          return c.title.toLowerCase().includes(q);
        }).slice(0, 10);
      }

      for (const tempC of temp) {
        for (const selectedC of this.selectedGenres) {
          if (tempC.id === selectedC.id) {
            tempC._selected = true;
            break;
          }
        }
      }

      this.filteredGenres = temp;
    });
  }

  async getData() {
    this.genres = this.initializedData.genres;
    this.filteredGenres = this.genres.slice(0, 10);
    for (const tempC of this.filteredGenres) {
      for (const selectedC of this.selectedGenres) {
        if (tempC.id === selectedC.id) {
          tempC._selected = true;
          break;
        }
      }
    }
  }

  queryChange() {
    this.queryChanges.next(this.query);
  }

  selectionChange(genre: Genre, value: boolean) {
    if (value) {
      const filtered = this.selectedGenres.filter(c => c.id === genre.id);
      if (!filtered.length) {
        this.selectedGenres.push(genre);
      }
    } else {
      this.selectedGenres = this.selectedGenres.filter(c => c.id !== genre.id);
    }
  }

  close(submit: boolean, result?: any) {
    this.dialogRef.close({
      submit,
      result,
    });
  }
}
