import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GenreSelectorModalComponent } from './genre-selector-modal.component';

describe('CategorySelectorModalComponent', () => {
  let component: GenreSelectorModalComponent;
  let fixture: ComponentFixture<GenreSelectorModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GenreSelectorModalComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(GenreSelectorModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
