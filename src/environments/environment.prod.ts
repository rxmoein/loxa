export const environment = {
  production: true,
  authServiceURL: 'https://services.beatkhor.com/auth',
  contentUrl: 'https://services.beatkhor.com/content',
  uploaderUrl: 'https://services.beatkhor.com/tusd/files/',
  storageUrl: 'https://services.beatkhor.com/storage',
};
