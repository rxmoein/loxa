export const environment = {
  production: false,
  // authServiceURL: 'http://localhost:2020/auth',
  authServiceURL: 'https://services.beatkhor.com/auth',
  contentUrl: 'https://services.beatkhor.com/content',
  uploaderUrl: 'https://services.beatkhor.com/tusd/files/',
  storageUrl: 'https://services.beatkhor.com/storage',
};
